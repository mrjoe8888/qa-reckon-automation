/*
 * @Date: 2019-09-04 
 * @LastEditors: Joseph Ocero
 * @LastEditTime: 
 * @Description: file content
 */
import { faker } from '@faker-js/faker';
import { randHexColorCode  } from "../common";

// ****************** FAMILY DATA *************** //
export const ADD_NEW_FAMILY_DATA = {
	name: `${faker.name.lastName()}`,
	postalCode: `${faker.address.zipCode().toString(8)}`
};

export const ADD_NEW_PARENT_DATA = {
	
	familyId: `${randHexColorCode(8)}-${randHexColorCode(4)}-${randHexColorCode(4)}-${randHexColorCode(4)}-${randHexColorCode(12)}`,
	favouriteColour: faker.color.human(),
	firstName: faker.name.firstName(),
	lastName: faker.name.lastName()
};

export const ADD_NEW_CHILD_DATA = {
	familyId: `${randHexColorCode(8)}-${randHexColorCode(4)}-${randHexColorCode(4)}-${randHexColorCode(4)}-${randHexColorCode(12)}`,
	firstName: faker.name.firstName(),
	lsstName: faker.name.lastName(),
	favouriteToy: faker.animal.dog()
};
