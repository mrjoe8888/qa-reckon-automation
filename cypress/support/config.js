/*
 * @Author: Joseph Ocero
 * @Date: 2021-09-07 
 * @LastEditors: Joseph Ocero
 * @LastEditTime:
 * @Description: file content
 */
import { faker } from '@faker-js/faker';
import * as Const_App from "./constants/app";

//json

import JSON_API from "./json/api/api-endpoint.json";

export {
	Const_App,
	faker, //all constants
	JSON_API,
};
