/* Creator: Joseph ocero
 * @LastEditors: 
 * @DateCreated: 2022-09-07
 * @LastEditTime: 
 * @Description: custom methods
 **/
export const randHexColorCode = (nth) => {
    return (Math.random() * 0xfffff * 1000000).toString(16).replace('.', '').slice(0, nth);
}
