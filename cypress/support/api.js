/**
 * @file this file is for the api
 */
 import { faker } from '@faker-js/faker';
import REQUESTBUILDER from './lib/api/RequestBuilder';
import {JSON_API, Const_App} from './config';

/**
 * new
 */
Cypress.Commands.add('apiRequest', (params) => {

	params.headers = {
		'Content-Type':"application/json",
		'Accept':"application/json"
	};
	return new REQUESTBUILDER(Cypress.env('backendUrl'), JSON_API, params).triggered();
});

Cypress.Commands.add('api_createFamily', (numParents, numChild, favouriteColour) => {

	 var payload, familyId;

	if ( numParents < 3 && numParents > 0){
		payload = Const_App.ADD_NEW_FAMILY_DATA
		cy.api_addNewFamily(JSON.stringify(payload)).then((response) => {
			for (let cnt = 0; cnt < numParents; cnt++) {
				payload = Const_App.ADD_NEW_PARENT_DATA
				payload['familyId'] = response.body.id
				payload['firstName'] = faker.name.firstName()
				payload['lastName'] = response.body.name
				familyId = response.body.id;
				console.log("FAMILY :" + JSON.stringify(response.body))
				if (Object.keys(favouriteColour).length == 1){
					payload['favouriteColour'] = favouriteColour.adult1
				} else{
					payload['favouriteColour'] = favouriteColour[`adult${cnt+1}`]
				}
				cy.api_addNewParent(JSON.stringify(payload)).then((response) => {
					console.log("PARENT :" + JSON.stringify(response.body))
					
				})
			}
			if (numChild > 0) {
				for (let cnt = 0; cnt < numChild; cnt++) {
					payload = Const_App.ADD_NEW_CHILD_DATA
					payload['familyId'] = familyId
					payload['firstName'] = faker.name.firstName()
					payload['lastName'] = Cypress.env('api-request').api_addNewFamily.name
					cy.api_addNewChild(JSON.stringify(payload)).then((response) => {
						console.log("Child :" + JSON.stringify(response.body))
						// console.log(familyId)
					})
				}
			}
			
		})

	} else {
		cy.log('Number of Parents should between 1 or 2 && Favourite colour is in array format with maximum of two values only')
	}
});




