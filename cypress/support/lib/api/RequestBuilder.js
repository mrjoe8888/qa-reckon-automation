/**
 * @file this is version 1 of the api for standardization
 * @since 1.0.0
 */

let logging = false;
class RequestBuilder {
  /**
   * @constructor when making a request
   * @param  {string} url url of the api
   * @param  {json} jsonData json data
   * @param  {json} jsonParams contains the parameters needed
   */
  constructor(url, jsonData, jsonParams) {
    this.url = url;
    this.jsonData = jsonData;
    this.jsonParams = jsonParams;
  }

  init() {
    return {
      method: this.method(this.jsonData, this.jsonParams.main, this.jsonParams.ext), // method
      url: `${this.url}${this.extension(
        this.jsonData,
        this.jsonParams.main,
        this.jsonParams.ext,
        this.jsonParams.params
      )}`, //url
      headers: this.jsonParams.headers, //headers
      body: this.checker(this.jsonParams.body, null), //body
    };
  }

  /**
   * @description triggers the api request and wrap response as api.response
   */
  triggered() {
    let jsonData = this.init();

    return cy.request(
      this.structure(jsonData.method, jsonData.url, jsonData.headers, jsonData.body),
      { timeout: 60000, log: true }
    );
  }

  /**
   * @description Construct the request for get
   * @param  {string} method method type
   * @param  {string} url url of the api
   * @param  {json} headers headers to send
   * @param  {json} body body of the request
   * @return request structure
   */
  structure(method, url, headers, body) {
    if (["POST", "PUT", "PATCH"].includes(method)) {
      return {
        method,
        url,
        body,
        headers,
        failOnStatusCode: false,
        log: logging,
      };
    } else if (["GET", "DELETE"].includes(method)) {
      return {
        method,
        url,
        headers,
        failOnStatusCode: false,
        log: logging,
      };
    }
  }

  /**
   * Get the method from api.json
   * @param  {string} jsonData json objects
   * @param  {string} main main api name
   * @param  {string} ext child api name
   * @return metho to use for request
   */
  method(jsonData, main, ext) {
    return jsonData[main][ext].method;
  }

  /**
   * @descript Get the api link
   * @param  {string} jsonData json objects
   * @param  {string} main main api name
   * @param  {string} ext child api name
   * @param  {string} params optional when the url needs parameters
   * @return extended url with data
   */
  extension(jsonData, main, ext, params) {
    let url = jsonData[main][ext].api,
      regs = url.match(/({)\w+(})/g);

    if (regs !== null) {
      regs.forEach((items) => {
        url = url.replace(items, params[items.match(/\{([^)]+)\}/)[1]]);
      });
    }
    return url;
  }

  /**
   * @description Checks if value arg0 exists. Uses arg0 if exists
   * @param  {string} arg0 variable to check
   * @param  {string} arg1 value to set if arg0 is undefined, null and empty
   * @return final data
   */
  checker(arg0, arg1) {
    return arg0 === undefined || arg0 === null || arg0 === "" ? arg1 : arg0;
  }
}
export default RequestBuilder;
