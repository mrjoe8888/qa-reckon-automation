/*
 * @Author: Joseph Ocero
 * @Date: 2021-09-07 
 * @LastEditors: Joseph Ocero
 * @LastEditTime:
 * @Description: file content
 */
/**
 * @module api_family
 * @description Post new Family
 */
 Cypress.Commands.add("api_addNewFamily", (body) => {
    cy.apiRequest({
          main: 'family',
          ext: 'addnewFamily',
          body: body,
      }).then((response) => {
          Cypress.env('api-request', {
              ...Cypress.env('api-request'),
              api_addNewFamily: response.body,
          });
      });
  });

/**
 * @module api_parent
 * @description Post new Parent
 */
 Cypress.Commands.add("api_addNewParent", (body) => {
    cy.apiRequest({
          main: 'family',
          ext: 'addnewParent',
          body: body,
      }).then((response) => {
          Cypress.env('api-request', {
              ...Cypress.env('api-request'),
              api_addNewParent: response.body,
          });
      });
  });

/**
 * @module api_getfamily_eligibility
 * @description Retrieve eligibility status of the family
 */
 Cypress.Commands.add("api_getFamilyEligibility", (familyId) => {
    cy.apiRequest({
          main: 'family',
          ext: 'getfamilyElegibility',
          params: {
            familyId: familyId
          },
      }).then((response) => {
          Cypress.env('api-request', {
              ...Cypress.env('api-request'),
              api_getFamilyEligibility: response.body,
          });
      });
  });

  /**
 * @module api_getfamilyById
 * @description Retrieve family data by ID
 */
 Cypress.Commands.add("api_getFamilyById", (familyId) => {
    cy.apiRequest({
          main: 'family',
          ext: 'getfamilyById',
          params: {
            familyId: familyId
          },
      }).then((response) => {
          Cypress.env('api-request', {
              ...Cypress.env('api-request'),
              api_getFamilyById: response.body,
          });
      });
  });