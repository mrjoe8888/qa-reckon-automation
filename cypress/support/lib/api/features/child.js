/*
 * @Author: Joseph Ocero
 * @Date: 2021-09-07 
 * @LastEditors: Joseph Ocero
 * @LastEditTime:
 * @Description: file content
 */
/**
 * @module api_addNewChild
 * @description Post new child in the family
 */
 Cypress.Commands.add("api_addNewChild", (body) => {
    cy.apiRequest({
          main: 'child',
          ext: 'addnewChild',
          body: body,
      }).then((response) => {
          Cypress.env('api-request', {
              ...Cypress.env('api-request'),
              api_addNewChild: response.body,
          });
      });
  });

/**
 * @module api_getChildById
 * @description: Retrieve Child by using ID
 */
 Cypress.Commands.add("api_getChildById", (childId) => {
    cy.apiRequest({
          main: 'child',
          ext: 'getchildById',
          params: {
            childId: childId
          },
      }).then((response) => {
          Cypress.env('api-request', {
              ...Cypress.env('api-request'),
              api_getChildById: response.body,
          });
      });
  });