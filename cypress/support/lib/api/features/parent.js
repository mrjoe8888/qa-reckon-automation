/*
 * @Author: Joseph Ocero
 * @Date: 2021-09-07 
 * @LastEditors: Joseph Ocero
 * @LastEditTime:
 * @Description: file content
 */
/**
 * @module api_family
 * @description Post new Family
 */
 Cypress.Commands.add("api_addNewParent", (body) => {
    cy.apiRequest({
          main: 'parent',
          ext: 'addnewParent',
          body: body,
      }).then((response) => {
          Cypress.env('api-request', {
              ...Cypress.env('api-request'),
              api_addNewParent: response.body,
          });
      });
  });

/**
 * @module api_addNewParent
 * @description Post new Parent
 */
 Cypress.Commands.add("api_getParentById", (parentId) => {
    cy.apiRequest({
          main: 'parent',
          ext: 'getparentById',
          params: { parentId: parentId },
      }).then((response) => {
          Cypress.env('api-request', {
              ...Cypress.env('api-request'),
              api_getParentById: response.body,
          });
      });
  });
