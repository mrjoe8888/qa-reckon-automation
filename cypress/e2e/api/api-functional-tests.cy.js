/* Creator: Joseph ocero
 * @LastEditors: 
 * @DateCreated: 2022-09-07
 * @LastEditTime: 
 * @Description: API Testing
 **/
/// <reference types="cypress" />

import { faker } from '@faker-js/faker';
import { Const_App } from "../../support/config";

describe("Family API services testing", function() { 
    var payload, familyId;

    it('Verify able to add new family', () => {
        payload = Const_App.ADD_NEW_FAMILY_DATA
        cy.api_addNewFamily(JSON.stringify(payload)).then((response) => {
            expect(response.status).to.be.equal(201);
            expect(response.body.name).to.be.equal(payload.name)
            expect(response.body.id.length).to.be.equal(36)
            
        })
    })
    it('Verify able to add new parents', () => {
        payload = Const_App.ADD_NEW_PARENT_DATA
        payload['familyId'] = Cypress.env('api-request').api_addNewFamily.id
        payload['lastName'] = Cypress.env('api-request').api_addNewFamily.name
        payload['favouriteColour'] = 'Yellow'
        cy.api_addNewParent(JSON.stringify(payload)).then((response) => {
            expect(response.status).to.be.equal(201);
            expect(response.body.familyId).to.be.equal(payload.familyId)
            expect(response.body.favouriteColour).to.be.equal(payload.favouriteColour)
            expect(response.body.firstName).to.be.equal(payload.firstName)
            expect(response.body.lastName).to.be.equal(payload.lastName)
            expect(response.body.familyId.length).to.be.equal(36)
            expect(response.body.id.length).to.be.equal(36)

            payload = Const_App.ADD_NEW_PARENT_DATA
            payload['familyId'] = Cypress.env('api-request').api_addNewFamily.id
            payload['firstName'] = faker.name.firstName()
            payload['lastName'] = Cypress.env('api-request').api_addNewFamily.name
            payload['favouriteColour'] = 'Yellow'
            cy.api_addNewParent(JSON.stringify(payload)).then((response) => {
                expect(response.status).to.be.equal(201);
                expect(response.body.familyId).to.be.equal(payload.familyId)
                expect(response.body.favouriteColour).to.be.equal(payload.favouriteColour)
                expect(response.body.firstName).to.be.equal(payload.firstName)
                expect(response.body.lastName).to.be.equal(payload.lastName)
                expect(response.body.familyId.length).to.be.equal(36)
                expect(response.body.id.length).to.be.equal(36)
            })

        })
    })
    it('Verify able to add new child', () => {
        payload = Const_App.ADD_NEW_CHILD_DATA
        payload['familyId'] = Cypress.env('api-request').api_addNewFamily.id
        payload['lastName'] = Cypress.env('api-request').api_addNewFamily.name
        cy.api_addNewChild(JSON.stringify(payload)).then((response) => {
            expect(response.status).to.be.equal(201);
            expect(response.body.familyId).to.be.equal(payload.familyId)
            expect(response.body.favouriteToy).to.be.equal(payload.favouriteToy)
            expect(response.body.firstName).to.be.equal(payload.firstName)
            expect(response.body.familyId.length).to.be.equal(36)
            expect(response.body.id.length).to.be.equal(36)
        })
    })
   
    it('Verify to retrieve parent by ID', () => {
        var parent = Cypress.env('api-request').api_addNewParent
        cy.api_getParentById(parent.id).then((response) => {
            expect(response.status).to.be.equal(200);
            expect(response.body.id).to.be.equal(parent.id)
            expect(response.body.favouriteColour).to.be.equal(parent.favouriteColour)
            expect(response.body.firstName).to.be.equal(parent.firstName)
            expect(response.body.lastName).to.be.equal(parent.lastName)
            expect(response.body.familyId.length).to.be.equal(36)
            expect(response.body.id.length).to.be.equal(36)
        })
    })

    it('Verify to retrieve family by ID', () => {
        var family = Cypress.env('api-request').api_addNewFamily
        cy.api_getFamilyById(family.id).then((response) => {
                console.log(`Newly Added family - ${JSON.stringify(family)}`)
                console.log(`Actual response body - ${JSON.stringify(response.body)}`)
                expect(response.status).to.be.equal(200)
                expect(response.body.name).to.be.equal(family.name)
                expect(response.body.postcode).to.be.equal(family.postcode)
                expect(response.body.id).to.be.equal(family.id)
                expect(response.body.id.length).to.be.equal(36)
        })
    })
    it('Verify to retrieve HTTP status not found for retrieving family by using doesn\'t exist ID', () => {
        familyId = "50277abd-f619-4b56-89bc-8f17001c1f11"
        cy.api_getFamilyById(familyId).then((response) => {
                console.log(`Actual response body - ${JSON.stringify(response.body)}`)
                expect(response.status).to.be.equal(404)
                
        })
    })
    it('Verify to retrieve HTTP status bad request for retrieving family by using invalid ID', () => {
        familyId = faker.database.mongodbObjectId()
        cy.api_getFamilyById(familyId).then((response) => {
                console.log(`Actual response body - ${JSON.stringify(response.body)}`)
                expect(response.status).to.be.equal(400)    
        })
    })
})