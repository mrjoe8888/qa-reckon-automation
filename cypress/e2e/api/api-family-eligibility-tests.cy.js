/* Creator: Joseph ocero
 * @LastEditors: 
 * @DateCreated: 2022-09-08
 * @LastEditTime: 
 * @Description: API Family eligibility testing to own a dog
 **/
/// <reference types="cypress" />

describe("Family API services testing", function() { 
    var numParents, numChild, favouriteColour;

    it('TC1: Verify a single parent with favourite colour is yellow and having a child can own a dog', () => {
        cy.api_createFamily(numParents = 1, numChild = 1, favouriteColour = {adult1: 'Yellow'}).then((response) => {
            var family = Cypress.env('api-request').api_addNewFamily
            cy.api_getFamilyEligibility(family.id).then((response) => {
                console.log(`TC1: Eligibility(Error) - ${JSON.stringify(response.body)}`)
                expect(response.status).to.be.equal(200);
                expect(response.body.allowedDog).to.be.equal('Y')
                expect(response.body.numberOfDogs).to.be.equal(1)
            })
        })
    })
    it('TC2: Verify a single parent with favourite colour is not yellow and having a child can\'t have a dog', () => {
        cy.api_createFamily(numParents = 1, numChild = 1, favouriteColour = {adult1: 'Red'}).then((response) => {
            var family = Cypress.env('api-request').api_addNewFamily
            cy.api_getFamilyEligibility(family.id).then((response) => {
                console.log(`TC2: Eligibility(Error) - ${JSON.stringify(response.body)}`)
                expect(response.status).to.be.equal(200);
                expect(response.body.allowedDog).to.be.equal('N')
                expect(response.body.numberOfDogs).to.be.equal(0)
            })
        })
    })
    it('TC3: Verify parents with one have a favourite colour is yellow and the other one is not and having a child can\'t have a dog', () => {
        cy.api_createFamily(numParents = 2, numChild = 1, favouriteColour = {adult1: 'Yellow', adult2: 'Blue'}).then((response) => {
            var family = Cypress.env('api-request').api_addNewFamily
            cy.api_getFamilyEligibility(family.id).then((response) => {
                console.log(`TC3: Eligibility(Error) - ${JSON.stringify(response.body)}`)
                expect(response.status).to.be.equal(200);
                expect(response.body.allowedDog).to.be.equal('N')
                expect(response.body.numberOfDogs).to.be.equal(0)
            })
        })
    })
    it('TC4: Verify both parents with favourite colour is yellow and having a child can have two dogs', () => {
        cy.api_createFamily(numParents = 2, numChild = 1, favouriteColour = {adult1: 'Yellow', adult2: 'Yellow'}).then((response) => {
            var family = Cypress.env('api-request').api_addNewFamily
            cy.api_getFamilyEligibility(family.id).then((response) => {
                expect(response.status).to.be.equal(200);
                expect(response.body.allowedDog).to.be.equal('Y')
                expect(response.body.numberOfDogs).to.be.equal(2)
            })
        })
    })
    it('TC5: Verify both parents with favourite colour aren\'t yellow and having a child can\'t own a dogs', () => {
        cy.api_createFamily(numParents = 2, numChild = 1, favouriteColour = {adult1: 'Brown', adult2: 'Black'}).then((response) => {
            var family = Cypress.env('api-request').api_addNewFamily
            cy.api_getFamilyEligibility(family.id).then((response) => {
                console.log(`TC5: Eligibility(Error) - ${JSON.stringify(response.body)}`)
                expect(response.status).to.be.equal(200);
                expect(response.body.allowedDog).to.be.equal('N')
                expect(response.body.numberOfDogs).to.be.equal(0)
            })
        })
    })
    it('TC6: Verify a single parent with favourite colour is yellow and having two children can\'t own a dog', () => {
        cy.api_createFamily(numParents = 1, numChild = 2, favouriteColour = {adult1: 'Yellow'}).then((response) => {
            var family = Cypress.env('api-request').api_addNewFamily
            cy.api_getFamilyEligibility(family.id).then((response) => {
                expect(response.status).to.be.equal(200);
                expect(response.body.allowedDog).to.be.equal('N')
                expect(response.body.numberOfDogs).to.be.equal(0)
            })
        })
    })
    it('TC7: Verify a single parent with favourite colour isn\'t yellow and having two children can\'t own a dog', () => {
        cy.api_createFamily(numParents = 1, numChild = 2, favouriteColour = {adult1: 'Pink'}).then((response) => {
            var family = Cypress.env('api-request').api_addNewFamily
            cy.api_getFamilyEligibility(family.id).then((response) => {
                expect(response.status).to.be.equal(200);
                expect(response.body.allowedDog).to.be.equal('N')
                expect(response.body.numberOfDogs).to.be.equal(0)
            })
        })
    })
    it('TC8: Verify parents with one parent have a favourite colour is yellow and the other is not and having two children can\'t have a dog', () => {
        cy.api_createFamily(numParents = 2, numChild = 2, favouriteColour = {adult1: 'Violet', adult2: 'Yellow'}).then((response) => {
            var family = Cypress.env('api-request').api_addNewFamily
            cy.api_getFamilyEligibility(family.id).then((response) => {
                console.log(`TC8: Eligibility(Error) - ${JSON.stringify(response.body)}`)
                expect(response.status).to.be.equal(200);
                expect(response.body.allowedDog).to.be.equal('N')
                expect(response.body.numberOfDogs).to.be.equal(0)
            })
        })
    })
    it('TC9: Verify both parents with favourite colour is yellow and having two children can have a dog', () => {
        cy.api_createFamily(numParents = 2, numChild = 2, favouriteColour = {adult1: 'Yellow', adult2: 'Yellow'}).then((response) => {
            var family = Cypress.env('api-request').api_addNewFamily
            cy.api_getFamilyEligibility(family.id).then((response) => {
                console.log(`TC9: Eligibility(Error) - ${JSON.stringify(response.body)}`)
                expect(response.status).to.be.equal(200);
                expect(response.body.allowedDog).to.be.equal('Y')
                expect(response.body.numberOfDogs).to.be.equal(1)
            })
        })
    })
    it('TC10: Verify both parents with favourite colour aren\'t yellow and having two children can have a dog', () => {
        cy.api_createFamily(numParents = 2, numChild = 2, favouriteColour = {adult1: 'Green', adult2: 'Green'}).then((response) => {
            var family = Cypress.env('api-request').api_addNewFamily
            cy.api_getFamilyEligibility(family.id).then((response) => {
                console.log(`TC10: Eligibility(Error) - ${JSON.stringify(response.body)}`)
                expect(response.status).to.be.equal(200);
                expect(response.body.allowedDog).to.be.equal('N')
                expect(response.body.numberOfDogs).to.be.equal(0)
            })
        })
    })
    it('TC11: Verify parents with one parent have a favourite colour is yellow and the other is not and having more than two children can\'t own a dog', () => {
        cy.api_createFamily(numParents = 2, numChild = 3, favouriteColour = {adult1: 'Yellow', adult2: 'Green'}).then((response) => {
            var family = Cypress.env('api-request').api_addNewFamily
            cy.api_getFamilyEligibility(family.id).then((response) => {
                expect(response.status).to.be.equal(200);
                expect(response.body.allowedDog).to.be.equal('N')
                expect(response.body.numberOfDogs).to.be.equal(0)
            })
        })
    })
    it('TC12: Verify both parents with favourite colour is yellow and having more than two children can\'t own a dog', () => {
        cy.api_createFamily(numParents = 2, numChild = 4, favouriteColour = {adult1: 'Yellow', adult2: 'Yellow'}).then((response) => {
            var family = Cypress.env('api-request').api_addNewFamily
            cy.api_getFamilyEligibility(family.id).then((response) => {
                console.log(`TC12: Eligibility(Error) - ${JSON.stringify(response.body)}`)
                expect(response.status).to.be.equal(200);
                expect(response.body.allowedDog).to.be.equal('N')
                expect(response.body.numberOfDogs).to.be.equal(0)
            })
        })
    })
    it('TC13: Verify both parents with favourite colour aren\'t yellow and having more than two children can\'t own a dog', () => {
        cy.api_createFamily(numParents = 2, numChild = 5, favouriteColour = {adult1: 'Black', adult2: 'Grey'}).then((response) => {
            var family = Cypress.env('api-request').api_addNewFamily
            cy.api_getFamilyEligibility(family.id).then((response) => {
                console.log(`TC13: Eligibility(Error) - ${JSON.stringify(response.body)}`)
                expect(response.status).to.be.equal(200);
                expect(response.body.allowedDog).to.be.equal('N')
                expect(response.body.numberOfDogs).to.be.equal(0)
            })
        })
    })
    it('TC14: Verify a widowed, divorced, separated marital status without a child with favourite colour is yellow can\'t own a dog', () => {
        cy.api_createFamily(numParents = 1, numChild = 0, favouriteColour = {adult1: 'Yellow'}).then((response) => {
            var family = Cypress.env('api-request').api_addNewFamily
            cy.api_getFamilyEligibility(family.id).then((response) => {
                expect(response.status).to.be.equal(200);
                expect(response.body.allowedDog).to.be.equal('N')
                expect(response.body.numberOfDogs).to.be.equal(0)
            })
        })
    })
    it('TC15: Verify a widowed, divorced, separated marital status without a child with favourite colour isn\'t yellow can\'t own a dog', () => {
        cy.api_createFamily(numParents = 1, numChild = 1, favouriteColour = {adult1: 'Grey'}).then((response) => {
            var family = Cypress.env('api-request').api_addNewFamily
            cy.api_getFamilyEligibility(family.id).then((response) => {
                console.log(`TC15: Eligibility(Error) - ${JSON.stringify(response.body)}`)
                expect(response.status).to.be.equal(200);
                expect(response.body.allowedDog).to.be.equal('N')
                expect(response.body.numberOfDogs).to.be.equal(0)
            })
        })
    })
    it('TC16: Verify a single parent with favourite colour is yellow and having more than two children can\'t own a dog', () => {
        cy.api_createFamily(numParents = 1, numChild = 6, favouriteColour = {adult1: 'Yellow'}).then((response) => {
            var family = Cypress.env('api-request').api_addNewFamily
            cy.api_getFamilyEligibility(family.id).then((response) => {
                expect(response.status).to.be.equal(200);
                expect(response.body.allowedDog).to.be.equal('N')
                expect(response.body.numberOfDogs).to.be.equal(0)
            })
        })
    })
    it('TC17: Verify a single parent with favourite colour isn\'t yellow and having more than two children can\'t own a dog', () => {
        cy.api_createFamily(numParents = 1, numChild = 3, favouriteColour = {adult1: 'Orange'}).then((response) => {
            var family = Cypress.env('api-request').api_addNewFamily
            cy.api_getFamilyEligibility(family.id).then((response) => {
                console.log(`TC17: Eligibility(Error) - ${JSON.stringify(response.body)}`)
                expect(response.status).to.be.equal(200);
                expect(response.body.allowedDog).to.be.equal('N')
                expect(response.body.numberOfDogs).to.be.equal(0)
            })
        })
    })
    it('TC18: Verify a wife and a husband with favourite colour is yellow and having no children can\'t own a dog', () => {
        cy.api_createFamily(numParents = 2, numChild = 0, favouriteColour = {adult1: 'Yellow', adult2: 'Yellow'}).then((response) => {
            var family = Cypress.env('api-request').api_addNewFamily
            cy.api_getFamilyEligibility(family.id).then((response) => {
                console.log(`TC18: Eligibility(Error) - ${JSON.stringify(response.body)}`)
                expect(response.status).to.be.equal(200);
                expect(response.body.allowedDog).to.be.equal('N')
                expect(response.body.numberOfDogs).to.be.equal(0)
            })
        })
    })
    it('TC19: Verify a wife and a husband with either one has favourite colour is yellow and the other one is not and having no children can\'t own a dog', () => {
        cy.api_createFamily(numParents = 2, numChild = 0, favouriteColour = {adult1: 'Yellow', adult2: 'Brown'}).then((response) => {
            var family = Cypress.env('api-request').api_addNewFamily
            cy.api_getFamilyEligibility(family.id).then((response) => {
                console.log(`TC19: Eligibility(Error) - ${JSON.stringify(response.body)}`)
                expect(response.status).to.be.equal(200);
                expect(response.body.allowedDog).to.be.equal('N')
                expect(response.body.numberOfDogs).to.be.equal(0)
            })
        })
    })
    it('TC20: Verify a wife and a husband with favourite colour isn\'t  yellow and having no children can\'t own a dog', () => {
        cy.api_createFamily(numParents = 2, numChild = 0, favouriteColour = {adult1: 'Pink', adult2: 'Red'}).then((response) => {
            var family = Cypress.env('api-request').api_addNewFamily
            cy.api_getFamilyEligibility(family.id).then((response) => {
                console.log(`TC20: Eligibility(Error) - ${JSON.stringify(response.body)}`)
                expect(response.status).to.be.equal(200);
                expect(response.body.allowedDog).to.be.equal('N')
                expect(response.body.numberOfDogs).to.be.equal(0)
            })
        })
    })

})