const { defineConfig } = require('cypress')

module.exports = defineConfig({
  defaultCommandTimeout: 60000,
  responseTimeout: 12000,
  requestTimeout: 13000,
  pageLoadTimeout: 50000,
  nodeVersion: 'system',
  video: false,
  watchForFileChanges: false,
  failOnStatusCode: false,
  chromeWebSecurity: false,
  e2e: {
    // We've imported your old cypress plugins here.
    // You may want to clean this up later by importing these.
    excludeSpecPattern: ['*.md', '*.ts', '*.DS_Store']
  },
  env: {
    backendUrl: 'https://join.reckon.com/'
  }
})
