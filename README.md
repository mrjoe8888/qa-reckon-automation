# QA Reckon Automation

Welcome to QA Reckon Automation.

## Folder Structure

- e2e - contains Functional test cases 

- plugins - deals with operations outside the application under tests 

- support - contains the custom commands and function libraries to support the automation

## Installation

- from the command(windows)/shell(linux), send the command "npm install". This will install all dependencies including the cypress. for references of the needed items to install are mentioned in package.json

## Requirements

As for those who wishes to use this automation, the IDE that we are using is Visual Studio Code.

- [GIT] - a free and open source distributed version control system. you need to install and configure to connect to github

- [Visual Studio] - A text editor suitable for any javascript coding
  Link : https://code.visualstudio.com/
  Installer: https://code.visualstudio.com/

++ [Extensions] - Once you have installed the VS Code, from the left navigation bar, open the extension icon (box icon) and it will show the list of the extensions. Please install the following
Beautify - This is to beautify the code
GitHub - Connects to GitHub repository
TODO Highlight - This will help you to comment the codes that are yet to be done

++ [VS Code Settings] - To change the settings on the VS Code, I would recommend to change the following settings
From File > Preferences > Settings, Navigate to Text Editor > Formatting and check the following options
- Format on Paste - automatically formats once you paste the code copied from other scripts
- Format on Save - automatically formats the code once you save the file

## Post-Requisite

After the cloning of this repository, please run "npm install" on windows and "sudo npm install" on linux / mac to install the dependencies.

## Running
To run the application, there are few ways to perform.

For UI view, just send the command 
"npm run cy:run". 
For details on the script, please check the package.json on script section.

For headless view,
- Run all specs  
   "npx cypress run"

- Run API 
   "npx cypress run ./cypress/e2e/api/*"

With specific browser,
- Firefox  
   "npx cypress run --browser firefox"
- Chrome 
   "npx cypress run --browser chrome"
- Microsoft Edge 
   "npx cypress run --browser edge"
- Electron 
   "npx cypress run"


With parallel execution,
send the command
"npm run cy:parallel-regression"

## Test Strategy - using Boundary Testing
- Family Eligibility scenarios:
   https://docs.google.com/spreadsheets/d/1GpAj8qokCYbGq0fVqMPC9mr9z5qhjj6lPlw8jvMxQFI/edit?usp=sharing
- All API endpoints:
   https://docs.google.com/spreadsheets/d/1GpAj8qokCYbGq0fVqMPC9mr9z5qhjj6lPlw8jvMxQFI/edit#gid=1494399095
## Test cases Creation

- For API
Application URL: https://join.reckon.com/

1. Verify a single parent with favourite colour is yellow and having a child can own a dog
2. Verify a single parent with favourite colour is not yellow and having a child can't have a dog
3. Verify parents with one have a favourite colour is yellow and the other one is not and having a child can't have a dog
4. Verify both parents with favourite colour is yellow and having a child can have two dogs
5. Verify both parents with favourite colour aren't yellow and having a child can't own a dog
6. Verify a single parent with favourite colour is yellow and having two children can't own a dog
7. Verify a single parent with favourite colour isn't yellow and having two children can't own a dog
8. Verify parents with one parent have a favourite colour is yellow and the other is not and having two children can't have a dog
9. Verify both parents with favourite colour is yellow and having two children can have a dog
10. Verify both parents with favourite colour aren't yellow and having two children can have a dog
11. Verify parents with one parent have a favourite colour is yellow and the other is not and having more than two children can't own a dog
12. Verify both parents with favourite colour is yellow and having more than two children can't own a dog
13. Verify both parents with favourite colour aren't yellow and having more than two children can't own a dog
14. Verify a widowed, divorced, separated marital status without a child with favourite colour is yellow can't own a dog
15. Verify a widowed, divorced, separated marital status without a child with favourite colour isn't yellow can't own a dog
16. Verify a single parent with favourite colour is yellow and having more than two children can't own a dog
17. Verify a single parent with favourite colour isn't yellow and having more than two children can't own a dog
18. Verify a wife and a husband with favourite colour is yellow and having no children can't own a dog
19. Verify a wife and a husband with either one has favourite colour is yellow and the other one is not and having no children can't own a dog
20. Verify a wife and a husband with favourite colour isn't  yellow and having no children can't own a dog

Functional API Test cases
1. Verify able to add new family
2. Verify able to add new parents
3. Verify able to add new child
4. Verify to retrieve parent by ID
5. Verify to retrieve family by ID
6. Verify to retrieve HTTP status not found for retrieving family by using doesn't exist ID
